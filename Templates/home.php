<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,user-scalable=0,initial-scale=1,maximum-scale=1">
    <meta name="description" content="<?=$this->get('description')?>">
    <title><?=$this->get('title')?></title>
  </head>
  <body>
    <h1><?=$this->get('title')?></h1>
    <p><?=$this->get('content')?></p>
    <hr>
    <code>
<?=$this->get('debug')?>
    </code>
  </body>
</html>