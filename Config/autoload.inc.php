<?php

// autoload register for classes

function autoload($obj) {

  // This is a controller
  if (preg_match('/controller/i', $obj)) {
    $file = ucfirst(strtolower($obj)).'.php';
    if (!file_exists(Conf::controllerPath.$file)) {
      Debug::append('Error while load controller : '.Conf::controllerPath.$file.' not found.');
    } else {
      require_once(Conf::controllerPath.$file);
      Debug::append('Autoload: controller '.$obj.' loaded successfully.');
    }
  }
  // This is a class
  else {
    $file = ucfirst(strtolower($obj)).'.class.php';
    if (!file_exists(Conf::classPath.$file)) {
      Debug::append('Error while load class : '.Conf::classPath.$file.' not found.');
    } else {
      require_once(Conf::classPath.$file);
      Debug::append('Autoload: class '.$obj.' loaded successfully.');
    }
  }
}

// Errors handler

function errorHandler($errno, $errstr, $errfile, $errline) {
  Debug::append('An error has occured : <i>' . $errstr . '</i>, in '.$errfile.' at line '.$errline);
}

function shutdownHandler() {
  Debug::append('ShutdownHandler function called.');
  if (Conf::debugOut) echo Debug::output();
}

spl_autoload_register('autoload');
set_error_handler('errorHandler', E_ALL);
register_shutdown_function('shutdownHandler');
?>