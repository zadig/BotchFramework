<?php

class Conf {

  // Dev
  const enabled   = TRUE;
  const debug     = TRUE;
  const debugOut  = FALSE;

  // Website
  const name      = 'MyWebSite';
  const protocol  = 'https';
  const url       = self::protocol . '://MyWebSite/';
  const useDefault= TRUE;

  // Database
  const dbDriver  = 'mysql';
  const dbUser    = 'user';
  const dbName    = 'database';
  const dbPass    = 'passwd';
  const dbHost    = 'localhost';

  // Path
  const rootPath        = '/home/zadig/projets/BotchFramework/BotchFramework/';
  const wwwPath         = self::rootPath . 'Public/';
  const classPath       = self::rootPath . 'Models/';
  const viewsPath       = self::rootPath . 'Views/';
  const controllerPath  = self::rootPath . 'Controllers/';
  const confPath        = self::rootPath . 'Config/';
  const templatePath    = self::rootPath . 'Templates/';
}
?>
