<?php

class Routes {

  public static $param      = 'page';
  public static $default    = 'home';
  public static $Routes     = array(
    'home'      =>  array('controller'  =>  'HomeController',
                          'type'        =>  'GET',
                          'required'    =>  array(),
                          'nav'         =>  array('text'    =>  'Home',
                                                  'title'   =>  'Homepage',
                                                  'active'  =>  TRUE
                                                  )
                          ),

  );

  public static $defaultRoute = array( 'controller'  =>  'DefaultController',
                                        'type'        =>  'mixed',
                                        'required'    =>  array());

  public static function get($route) {
    if (array_key_exists($route, self::$Routes)) {
      return self::$Routes[$route];
    } else {
      Throw new Error('Unable to find a route for '.$route.'.', 404);
    }
  }
}
?>
