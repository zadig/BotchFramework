# BOTCH FRAMEWORK
## Présentation
Botch Framework est un framework simplifié et facile d'utilisation.
Botch Framework peut vous être utile pour l'installation de n'importe quel site internet.
Son utilisation repose sur une architecture simple et facile à modifier.

## Principe et fonctionnement
Passant tout d'abord par un controlleur général abstrait `GeneralController`, la demande de l'utilisateur suit une simple feuille de route `RouteController`.
Les définitions des différents points externes se fait sous forme d'un array très simple :
```php
protected static $Routes = array(
    'home'      =>  array('controller'  =>  'HomeController',
                          'type'        =>  'GET',
                          'required'    =>  array()
                          )
);
```

## Installation
```bash
git clone https://gitlab.botch.io/root/botch-framework.git www/
cd www/
```
Votre répertoire est prêt.
Veillez à éditer le fichier `Conf/conf.inc.php` et `Conf/routes.inc.php`.

## Important
### Les noms de fichiers
Les **controllers** doivent se situer dans les fichiers de syntaxe `Nomcontroller.php` : une **M**ajuscule en début de nom, et finissant par *controller*.

#### Exemple
```php
<?php

class CactusController {
?>
```
Ce fichier devra s'appeler `Cactuscontroller.php`, et se situer dans `Controllers/`.


Les **classes** doivent se situer dans les fichiers de syntaxe `Nom.class.php` : une **M**ajuscule en début de nom, et finissant par *.class*.

#### Exemple
```php
<?php

class Cactus  {
?>
```
Ce fichier devra s'appeler `Cactus.class.php`, et se situer dans `Models/`.

## Example
Dans cet exemple, je possède un fichier `accueil.html` et un fichier `contact.html` que je souhaiterais afficher à l'utilisateur.

#### Conf/routes.inc.php
```php
<?php

class Routes {

  public static $param     = 'page';
  public static $default   = 'accueil';
  protected static $Routes = array(
    'accueil'      =>  array('controller'   =>  'AccueilController',
                             'type'         =>  'GET',
                             'required'     =>  array()
                             ),

    'contact'      =>  array('controller'   =>  'ContactController',
                             'type'         =>  'GET',
                             'required'     =>  array()
                             )
  );

  public static function get($route) {
    if (array_key_exists($route, self::$Routes)) {
      return self::$Routes[$route];
    } else {
      Throw new Error('Unable to find a route for '.$route.'.','Route',404);
    }
  }
}
?>
```

#### Controllers/Accueilcontroller.php
```php
<?php
class AccueilController Extends GeneralController {

  public $template;
  public function __construct() {
    $this->template = new Template;
    $this->template->import('accueil.html');
    $this->template->flush();
  }
}
?>
```

#### Controllers/Contactcontroller.php
```php
<?php
class ContactController Extends GeneralController {

  public $template;
  public function __construct() {
    $this->template = new Template;
    $this->template->import('contact.html');
    $this->template->flush();
  }
}
?>
```