<?php

class MySQL {

  public static $pdo = NULL;

  public static function init() {
     try {
      self::$pdo = new \PDO('mysql:host=' . Conf::dbHost . ';dbname=' .
                              Conf::dbName,
                             Conf::dbUser, Conf::dbPass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
      Throw new Exception('MySQL::init: failed to connect to MySQL server ('.$e->getMessage().')');
    }
  }

  public static function select($table, $row, $conditions, $markers = NULL) {
    if (!isset(self::$pdo)) {
      self::init();
    }
    foreach($row as $k=>$v) {
      if (!preg_match('/^`/',$v)) {
        $row[$k] = '`'.$v.'`';
      }
    }
    $sql  = 'SELECT '.implode(',', $row).' FROM `'.$table.'` '.$conditions.';';
    if ($markers != NULL) {
      $sth  = self::$pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    } else {
      $sth  = self::$pdo->prepare($sql);
    }
    try {
      if ($markers != NULL) {
        $sth->execute($markers);
      } else {
        $sth->execute();
      }
    } catch (Exception $e) {
      Throw new Exception('MySQL::select: error during select query:'.$e->getMessage());
    }
    return $sth;
  }

  public static function insert($table, $data) {
    if (!isset(self::$pdo)) {
      self::init();
    }
    $rows = array_keys($data);
    foreach($rows as $k=>$v) {
      if (!preg_match('/^`/',$v)) {
        $rows[$k] = '`'.$v.'`';
        $data[$v] = self::typeof($data[$v]);
      }
    }
    $sql = 'INSERT INTO `'.$table.'`('.implode(',', $rows).') VALUES('.implode(',', $data).');';
    $sth = self::$pdo->prepare($sql);
    try {
      $sth->execute();
    } catch (Exception $e) {
      Throw new Exception('MySQL::insert: error during insert query:'.$e->getMessage());
    }
    return self::$pdo->lastInsertId();
  }

  public static function typeof($v) {
    if (preg_match('/^#(.+)$/', $v, $matches)) {
      return $matches[1];
    } elseif (is_string($v)) {
      return '"'.addslashes($v).'"';
    } elseif (is_int($v)) {
      return intval($v);
    }
  }

  public static function update($table, $data, $conditions, $markers = NULL) {
    if (!isset(self::$pdo)) {
      self::init();
    }
    $rows = array_keys($data);
    foreach($rows as $k=>$v) {
      if (!preg_match('/^`/',$v)) {
        $rows[$k] = '`'.$v.'`='.self::typeof($data[$v]);
      }
    }
    $sql = 'UPDATE `'.$table.'` SET '.implode(',', $rows).' '.$conditions.';';
    if ($markers != NULL) {
      $sth  = self::$pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    } else {
      $sth  = self::$pdo->prepare($sql);
    }
    try {
      if ($markers != NULL) {
        $sth->execute($markers);
      } else {
        $sth->execute();
      }
    } catch (Exception $e) {
      Throw new Exception('MySQL::update: error during update query:'.$e->getMessage());
    }
  }

  public static function delete($table, $conditions, $markers = NULL) {
    if (!isset(self::$pdo)) {
      self::init();
    }
    $sql = 'DELETE FROM `'.$table.'` '.$conditions.';';
    if ($markers != NULL) {
      $sth  = self::$pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    } else {
      $sth  = self::$pdo->prepare($sql);
    }
    try {
      if ($markers != NULL) {
        $sth->execute($markers);
      } else {
        $sth->execute();
      }
    } catch (Exception $e) {
      Throw new Exception('MySQL::delete: error during delete query:'.$e->getMessage());
    }
  }
}
?>
