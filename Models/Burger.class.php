<?php

class Burger {

  public function __construct() {
    $args = func_get_args();

    switch (func_num_args()) {

      case 1:
        // init by id
        $this->initById($args[0]);
        break;

      case 3:
        // init by new
        $this->create($args[0], $args[1], $args[2]);
        break;
    }
  }

  public function initById($id) {
    $sth = MySQL::select('commandes_burgers', array('nom', 'prix', 'actif'), 'WHERE id=:id', array(':id'=>intval($id)));
    if ($sth->rowCount() != 1) {
      Throw new Exception('Burger inexistant.');
    }
    $burger     = $sth->fetch();
    $this->id   = $id;
    $this->nom  = $burger['nom'];
    $this->prix = $burger['prix'];
    $this->actif= $burger['actif'];
  }

  public function create($nom, $prix, $actif) {
    $sth = MySQL::select('commandes_burgers', array('id'), 'WHERE nom=:nom', array(':nom'=>htmlspecialchars($nom)));
    if ($sth->rowCount() == 1) {
      Throw new Exception('Burger déjà existant.');
    }
    $this->id   = MySQL::insert('commandes_burgers', array('nom'=>htmlspecialchars($nom), 'prix'=>floatval($prix), 'actif'=>$actif));
    $this->nom  = htmlspecialchars($nom);
    $this->prix = floatval($prix);
    $this->actif= $actif;
  }

  public function save() {
    MySQL::update('commandes_burgers', array('nom'=>htmlspecialchars($this->nom), 'prix'=>floatval($this->prix), 'actif'=>$this->actif), 'WHERE id=:id', array(':id'=>intval($this->id)));
  }
}
?>
