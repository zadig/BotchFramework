<?php

class Error Extends Exception {

  public $code, $message;
  public function __construct($message, $code = NULL) {
    $this->message = $message;
    $this->code    = $code;
  }
}
?>
