<?php

class User {

  protected static $ip;
  protected static $country;
  protected static $userAgent;
  protected static $initialized = FALSE;

  private static function init() {
    static::$ip         = $_SERVER['REMOTE_ADDR'];
    static::$country    = geoip_country_code_by_name(static::$ip);
    if (!static::$country) static::$country = 'unknown';
    static::$userAgent  = $_SERVER['HTTP_USER_AGENT'];
    static::$initialized = TRUE;
  }

  public function getIp() {
    if (!static::$initialized) static::init();
    return static::$ip;
  }

  public function getCountry() {
    if (!static::$initialized) static::init();
    return static::$country;
  }

  public function getUserAgent() {
    if (!static::$initialized) static::init();
    return static::$userAgent;
  }
}
?>