<?php

class Data {

  public static $burgers, $commandes, $horaires;
  public static $init = FALSE;

  public static function init() {
    if (self::$init) return FALSE;

    $sth = MySQL::select('commandes_burgers', array('id', 'nom', 'prix', 'actif'), 'ORDER BY id ASC');
    self::$burgers = array();
    while ($r = $sth->fetch()) {
      self::$burgers[$r['id']] = $r;
    }


    $sth = MySQL::select('commandes_horaires', array('id', 'heure'), 'ORDER BY id ASC');
    self::$horaires = array();
    while ($r = $sth->fetch()) {
      self::$horaires[$r['id']] = self::$horaires[$r['heure']] = $r;
    }
  }
}
?>
