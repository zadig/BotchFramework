<?php

class Template {

  public $file;
  private $type = 'dynamic';

  public function import($file) {
    $this->file = Conf::templatePath.$file;

    if (!file_exists($this->file)) {
      Debug::append('Error while initializing new template : '.$this->file.' not found.');
    }
  }

  public function setType($type) {
    if (in_array($type, array('dynamic',''))) {
      $this->type = $type;
    } else {
      Debug::append('Unknown type for template : '.$type);
    }
  }

  public function flush() {
    require_once($this->file);
  }

  public function get($var, $specialChars = TRUE) {
    if (is_array($this->$var)) return $this->$var;
    return ($specialChars)?htmlspecialchars($this->$var):$this->$var;
  }

  public function set($vars) {
    foreach($vars as $k=>$v) {
      $this->$k = $v;
    }
  }
}