<?php

class HTML {
  public $end = "\r\n";

  public function __construct($title = NULL, $description = NULL, $template = NULL, $extra = array('css'=>array(), 'js'=>array(), 'meta'=>array()), $nav = TRUE) {
    $this->html = file_get_contents(Conf::viewsPath.'head.html');
    foreach($extra as $type=>$values) {
      foreach($values as $v) {
        switch ($type) {
          case 'css':
            $this->html .= '    <link rel="stylesheet" href="'.htmlspecialchars($v).'">'.$this->end;
            break;

          case 'js':
            $this->html .= '    <script src="'.htmlspecialchars($v).'"></script>'.$this->end;
            break;

          case 'meta':
            $this->html .= '    <meta name="'.$v['name'].'" content="'.$v['content'].'">'.$this->end;
            break;
        }
      }
    }

    if ($title != NULL) {
      $this->html .= '    <title>'.$title.'</title>'.$this->end;
    }

    if ($description != NULL) {
      $this->html .= '    <meta name="description" content="'.$description.'">'.$this->end;
    }

    $this->html .= '  </head>'.$this->end;
    if ($nav) {
      $this->generateNav();
    }
    if ($template != NULL) {
      $this->html .= file_get_contents($template);
    }
  }

  public function generateNav($title = NULL) {
    if ($title == NULL) {
      $title = Conf::name;
    }
    $this->html .= file_get_contents(Conf::viewsPath.'nav.html');
    $links = '';
    foreach(Routes::$Routes as $query=>$v) {
      $links .= '            <li'.((RouteController::$query == $query)?' class="active"':'').'><a href="/'.htmlspecialchars($query).'" title="'.$v['nav']['title'].'">'.$v['nav']['text'].'</a></li>'.$this->end;
    }
    $this->html = str_replace('{_LINKS_}', $links, $this->html);
    $this->html = str_replace('{_TITLE_}', $title, $this->html);
  }


  public function flush() {
    echo $this->html;
    die();
  }
}
