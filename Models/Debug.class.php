<?php

class Debug {

  protected static $debugBuffer = array();


  public static function append($trace) {
    array_push(self::$debugBuffer, array( 'timestamp' =>  microtime(TRUE),
                                          'trace'     =>  $trace
                                        ));
  }

  public static function load() {
    return self::$debugBuffer;
  }

  public static function output() {
    $output = '<div id="debugTrace">' .
              '  <h2>Debug Trace</h2>' .
              '  <pre>';
    foreach(self::$debugBuffer as $d) {
      $output .= '<b>' . date('H:i:s\.',$d['timestamp']) . preg_replace('/^0\./','',$d['timestamp']-intval($d['timestamp'])).'</b> : <i>' . $d['trace'] . '</i>' . "\r\n";
    }
    $output .= '  </pre></div>';
    return $output;
  }
}
?>