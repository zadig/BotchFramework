<?php

class Utils {

  public static function parseParameters($request, $name, $type, $regex = False, $pattern = NULL) {
    $request     = strtolower($request);
    $type        = strtolower($type);
    $arraySearch = ($request=='get')?$_GET:$_POST;

    // Parameter does not exist.
    if (!array_key_exists($name, $arraySearch)) return NULL;

    $value = $arraySearch[$name];

    if (!$regex) {
      switch ($type) {

        case 'int':
          return intval($value);

        case 'float':
          return floatval($value);

        case 'str':
          return strval($value);

        default:
          return $value;
      }
    } else {
      if (preg_match($pattern, $value, $matches)) {
        return $matches[1];
      } else {
        return NULL;
      }
    }
  }

  public static function randomstr($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  public static function mail_utf8($to, $from_user, $from_email, $sender, $subject = '(No subject)', $message = '') {
      $from_user = "=?UTF-8?B?".base64_encode($from_user)."?=";
      $subject = "=?UTF-8?B?".base64_encode($subject)."?=";

      $headers = "From: ".$from_user." <".$from_email.">\r\n".
                 "Reply-To: ".$from_email."\r\n".
               "MIME-Version: 1.0" . "\r\n" .
               "Content-type: text/html; charset=UTF-8" . "\r\n";

     return mail($to, $subject, $message, $headers, '-r '.$sender);
  }

  public static function exitWithCode($code, $message = '') {
    Header('HTTP/1.1 '.$code.' '.$message);
    die();
  }

}
?>
