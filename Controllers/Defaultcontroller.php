<?php

class DefaultController Extends GeneralController {

  public function __construct($route) {
    $route      = strtolower($route);
    $testFiles  = array('php'=>Conf::templatePath . $route . '.php', 'html'=>Conf::templatePath . $route . '.html');
    $template   = NULL;
    foreach($testFiles as $k=>$t) {
      if (file_exists($t)) {
        $template = $t;
        $type     = $k;
        break;
      }
    }
    if ($template == NULL) {
      if ($route != 404) {
        RouteController::redirect('404');
      } else {
        Utils::exitWithCode('404','Not Found');
      }
    } else {
      if ($type == 'php') {
        require_once($template);
      } else {
        echo file_get_contents($template);
      }
    }
  }
}
?>
