<?php

class RouteController Extends GeneralController {

  public static $query, $route;

  public static function init() {
    self::$query = Utils::parseParameters('GET',Routes::$param,'mixed');
    if (self::$query == NULL) {
      Header('HTTP/1.1 301 Permanently');
      Header('Location: /'.Routes::$default);

      // THIS :
      die();

      // OR
      self::$query = Routes::$default;
    }
    self::$route   = Routes::get(self::$query);

    // Check type
    switch (self::$route['type']) {

      case 'GET':
        if (count($_POST) != 0) {
          self::redirect();
          break;
        }
        break;

      case 'POST':
        if (count($_GET) != 0) {
          self::redirect();
          break;
        }
        break;
    }

    foreach(self::$route['required'] as $p) {
      if (self::$route['type'] == 'mixed') break;
      if (Utils::parseParameters(self::$route['type'],$p,'mixed') == NULL) {
        self::redirect();
        break;
      }
    }
    parent::$controller = new self::$route['controller']();
  }

  public static function redirect($path = '') {
    Header('HTTP/1.1 302 Found');
    Header('Location: /'.$path);
    die();
  }
}
?>
