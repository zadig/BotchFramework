<?php

require_once(__DIR__.'/../Config/conf.inc.php');
require_once(Conf::confPath.'autoload.inc.php');
require_once(Conf::confPath.'routes.inc.php');

try {
  RouteController::init();
} catch (Error $e) {
  if (Conf::useDefault) {
    $controller = Routes::$defaultRoute['controller'];
    $Page = new $controller(RouteController::$query);
  } else {
    Utils::exitWithCode(404, 'Not Found');
  }
}
?>
